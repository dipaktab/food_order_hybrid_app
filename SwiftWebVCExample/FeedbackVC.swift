//
//  FeedbackVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 13/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class FeedbackVC: UIViewController {

    @IBOutlet weak var txfFeedback: CustomTextField!
    @IBOutlet weak var btnSendFeedback: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var prefersStatusBarHidden: Bool {
          return true
      }
      
      override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
        hideKeyboard()
      }
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        sender.spring()
        self.dismissSemiModalView()
    }
    
    @IBAction func appExperienceButtonAction(_ sender: UIButton) {
        sender.spring()
        setBorderColor(sender)
    }
    
    @IBAction func paymentButtonAction(_ sender: UIButton) {
        sender.spring()
        setBorderColor(sender)

    }
    
    @IBAction func otherButtonAction(_ sender: UIButton) {
        sender.spring()
        setBorderColor(sender)
    }
    
    @IBAction func sendFeedbackButtonAction(_ sender:  UIButton) {
           sender.spring()
        hideKeyboard()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showAlert("", message: TSConstant.SentFeedbackMessage)
        self.dismissSemiModalView()
    }
    
    
    private func setBorderColor( _ onButton: UIButton) {
        
        onButton.isSelected = !onButton.isSelected
        if onButton.isSelected {
            onButton.layer.borderColor = UIColor.systemOrange.cgColor
        } else {
            onButton.layer.borderColor = UIColor.clear.cgColor
        }
        
    }
    
    private func hideKeyboard() {
        view.endEditing(true)
    }
    
    
     @IBAction func textFieldEditingChanged(_ textField: UITextField) {
         
         if let text = textField.text, text.count > 0 {
             btnSendFeedback.isUserInteractionEnabled = true
             btnSendFeedback.backgroundColor = .orange
         } else {
             btnSendFeedback.isUserInteractionEnabled = false
             if #available(iOS 13.0, *) {
                 btnSendFeedback.backgroundColor = .systemGray2
             } else {
                 btnSendFeedback.backgroundColor = .gray
             }
         }
     }
    
    
    
    

}
