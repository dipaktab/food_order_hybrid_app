//
//  AddNewAddressVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 13/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class AddNewAddressVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSaveAddress: UIButton!
    
    let titles = ["Name", "Address line 1", "Address line 2", "City", "State", "Country", "Postal Code"]
    let placeHolder = ["e.g Home / Work", "Building, Floor", "Streat", "Enter your city", "Enter your State", "Enter your Country", "Enter your Postal Code"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideKeyboard()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    private func setupTableView() {
        
        tableView.register(UINib(nibName: String(describing: AddAddressCell.self), bundle: nil), forCellReuseIdentifier: String(describing: AddAddressCell.self))
        self.tableView.tableFooterView = UIView()
        tableView.reloadData()
    }
    
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        sender.spring()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAddressAction(_ sender: UIButton) {
        sender.spring()
        saveAddress()
    }
    
    func isValidAddress() -> Bool {
        
        var isValidName = false
        if let nameCell = tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? AddAddressCell, let name = nameCell.txfAddress.text, name.count > 0 {
            isValidName = true
        }
        
        var isValidAddress = false
        if let addressCell = tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as? AddAddressCell, let address = addressCell.txfAddress.text, address.count > 0 {
            isValidAddress = true
        }
        
        var isValid = false
        if isValidAddress && isValidName {
            isValid = true
        }
        
        enableSaveAddressButton(isValid)
        return isValid
    }
    
    
    private func enableSaveAddressButton( _ isEnable: Bool) {
        
        if isEnable {
            btnSaveAddress.isUserInteractionEnabled = true
            btnSaveAddress.backgroundColor = .orange
        } else {
            btnSaveAddress.isUserInteractionEnabled = false
            if #available(iOS 13.0, *) {
                btnSaveAddress.backgroundColor = .systemGray2
            } else {
                btnSaveAddress.backgroundColor = .gray
            }
        }
    }
    
    private func saveAddress() {
        
        hideKeyboard()
        
        if isValidAddress() {
            
            var addressString = ""
            var name = ""
            for index in 0..<titles.count {
                
                if let cell = tableView.cellForRow(at: IndexPath.init(row: index, section: 0)) as? AddAddressCell, let text = cell.txfAddress.text, text.count > 0 {
                    
                    if index == 0 {
                        name = text
                    } else {
                        addressString = addressString + text
                    }
                    
                    if index > 0 && index != (titles.count - 1) {
                        addressString = addressString + ", "
                    }
                }
            }
            
            if !addressString.isEmpty {
                
                var index = 99999
                
                if TSConstant.AddressTitleHome.caseInsensitiveCompare(name) == ComparisonResult.orderedSame
                {
                    index = 0
                } else if TSConstant.AddressTitleWork.caseInsensitiveCompare(name) == ComparisonResult.orderedSame
                {
                    index = 1
                }
                
                let address = Address.init(title: name, fullAddress: addressString, groupName: TSConstant.AddressGroupFavorites, index: index)
                
                UserDefaults.standard.saveAddress(address)
                showAlert("", message: TSConstant.MessageAddressSaved)
            }
        }
        
    }
    
    private func showAlert(_ title: String, message: String) {
        var titleString = title
        if title.isEmpty {
            titleString = TSConstant.DefaultAlertTitle
        }
        let alert = UIAlertController(title: titleString, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true)
    }
    
}



extension AddNewAddressVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddAddressCell.self), for: indexPath) as! AddAddressCell
        
        cell.lblTitle.text = titles[indexPath.row]
        cell.txfAddress.placeholder = placeHolder[indexPath.row]
        if indexPath.row == 0 || indexPath.row == 1 {
            cell.lblRequiredFiled.isHidden = false
        }
        cell.txfAddress.delegate = self
        cell.delegate = self

        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
}

extension AddNewAddressVC: AddAddressCellDelegate {
    
    func keybaordDoneButtonClicked() {
        saveAddress()
    }
}


extension AddNewAddressVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        btnSaveAddress.isEnabled = isValidAddress()
    }
    
    private func hideKeyboard() {
        view.endEditing(true)
    }
}
