//
//  AddressCell.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 12/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class AddressCell: UITableViewCell {

    @IBOutlet weak var imgvAddressIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblFullAddress: UILabel!
    @IBOutlet weak var imvLeadingConstr: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell( _ address: Address) {
        
        lblName.text = address.title
        lblFullAddress.text = address.fullAddress
        
        if address.title == TSConstant.AddressTitleAddNew {
            imvLeadingConstr.constant -= imgvAddressIcon.frame.size.width + imgvAddressIcon.frame.origin.x
            lblName.textColor = .systemOrange
            self.accessoryType = .disclosureIndicator
        } else {
            imgvAddressIcon.backgroundColor = .systemOrange
            lblName.textColor = .darkText
            imvLeadingConstr.constant = 16

        }
        
    }
    
}
