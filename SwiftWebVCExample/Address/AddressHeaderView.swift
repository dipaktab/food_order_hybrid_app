//
//  AddressHeaderView.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 12/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class AddressHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var lblTitle: UILabel!

}
