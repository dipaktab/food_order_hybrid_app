//
//  ServedPlacesVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 12/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class ServedPlacesVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var favorites = [Address]()
    var others = [Address]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        sender.spring()
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setupTableView() {

        let nib = UINib(nibName: String(describing: AddressHeaderView.self), bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: String(describing: AddressHeaderView.self))
        tableView.register(UINib(nibName: String(describing: AddressCell.self), bundle: nil), forCellReuseIdentifier: String(describing: AddressCell.self))
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView()
    }
    
    func sortWithKeys(_ dict: [String: Any]) -> [String: [Address]]
    {
        let sorted = dict.sorted(by: { $0.key < $1.key })
        var newDict: [String: [Address]] = [:]
        for sortedDict in sorted {
            newDict[sortedDict.key] = sortedDict.value as? [Address]
        }
        return newDict

    }
    
    private func loadData() {
        
        let addressArray = UserDefaults.standard.getAddresses()
        
        let addresses = Dictionary(grouping: addressArray, by: { (element: Address) in
            return element.groupName
        })
        
        favorites = addresses[TSConstant.AddressGroupFavorites] ?? []
        if favorites.count > 0 {
            favorites.sort(by: { $0.index < $1.index })
        }
        
        others = addresses[TSConstant.AddressGroupOthers] ?? []
        
        if others.count > 0 {
            others.sort(by: { $0.index < $1.index })
        }
        
        tableView.reloadData()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ServedPlacesVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var sections = 1
        if favorites.count > 0 {
            sections += 1
        }
        return sections
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if favorites.count > 0 && section == 0 {
            return favorites.count
        }
        return others.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddressCell.self), for: indexPath) as! AddressCell
        
        var address = Address()
        if favorites.count > 0 && indexPath.section == 0 {
            address = favorites[indexPath.row]
        } else {
            address = others[indexPath.row]
        }

        cell.configureCell(address)
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        var section = 0
        if favorites.count > 0 {
            section = 1
        }
        
        if indexPath.section == section && indexPath.row == others.count - 1 {
            self.performSegue(withIdentifier: "showAddNewAddressVC", sender: self)
        }
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let header = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: AddressHeaderView.self)) as! AddressHeaderView
        
        var title = TSConstant.AddressGroupOthers
        if favorites.count > 0 && section == 0 {
            title = TSConstant.AddressGroupFavorites
        }
        
        header.lblTitle.text = title
        return header
    }
    
}
