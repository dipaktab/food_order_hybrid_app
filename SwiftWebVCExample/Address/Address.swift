//
//  Address.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 12/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import Foundation

struct Address: Codable {
    var title = ""
    var fullAddress = ""
    var groupName = ""
    var index = 99999
}
