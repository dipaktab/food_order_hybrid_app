//
//  AddAddressCell.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 13/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

protocol AddAddressCellDelegate: class {
    func keybaordDoneButtonClicked()
}

class AddAddressCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRequiredFiled: UILabel!
    @IBOutlet weak var txfAddress: UITextField!
    
    weak var delegate: AddAddressCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txfAddress.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @objc func doneButtonClicked(_ sender: Any) {
        delegate?.keybaordDoneButtonClicked()
    }
}
