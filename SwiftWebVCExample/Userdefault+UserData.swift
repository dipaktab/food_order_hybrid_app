//
//  Userdefault+UserData.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 10/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import Foundation
import UIKit



enum UserDefaultsKeys : String {
    case FirstName
    case LastName
    case Email
    case MobileNumber
    case DOB
    case Gender
    case ProfilePic
    case Addresses

}

extension UserDefaults {
    
    //MARK: Save User Data
    func setFirstName(value: String) {
        set(value, forKey: UserDefaultsKeys.FirstName.rawValue)
    }
    
    func getFirstName() -> String {
        return string(forKey: UserDefaultsKeys.FirstName.rawValue) ?? ""
    }
    
    func setLastName(value: String) {
        set(value, forKey: UserDefaultsKeys.LastName.rawValue)
    }
    
    func getLastName() -> String {
        return string(forKey: UserDefaultsKeys.LastName.rawValue) ?? ""
    }
    
    func setEmail(value: String) {
        set(value, forKey: UserDefaultsKeys.Email.rawValue)
    }
    
    func getEmail() -> String {
        return string(forKey: UserDefaultsKeys.Email.rawValue) ?? ""
    }
    
    func setMobileNumber(value: String) {
        set(value, forKey: UserDefaultsKeys.MobileNumber.rawValue)
    }
    
    func getMobileNumber() -> String {
        return string(forKey: UserDefaultsKeys.MobileNumber.rawValue) ?? ""
    }
    
    
    func setDOB(value: String) {
        set(value, forKey: UserDefaultsKeys.DOB.rawValue)
    }
    
    func getDOB() -> String {
        return string(forKey: UserDefaultsKeys.DOB.rawValue) ?? ""
    }
    
    func setGender(value: String) {
        set(value, forKey: UserDefaultsKeys.Gender.rawValue)
    }
    
    func getGender() -> String {
        return string(forKey: UserDefaultsKeys.Gender.rawValue) ?? TSConstant.Male
    }
    
    func saveProfilePic(image: UIImage?) {
        if let img = image, let pngRepresentation = UIImagePNGRepresentation(img) {
            UserDefaults.standard.set(pngRepresentation,
                                        forKey: UserDefaultsKeys.ProfilePic.rawValue)
        }
    }
    
    func retrieveProfilePic() -> UIImage? {
      if let imageData = UserDefaults.standard.object(forKey: UserDefaultsKeys.ProfilePic.rawValue) as? Data,
            let image = UIImage(data: imageData) {
            return image
        }
        return nil
    }
    

    func saveAddress(_ address: Address) {
        
        var addresses = getAddresses()
        addresses.append(address)
        UserDefaults.standard.set(try? PropertyListEncoder().encode(addresses), forKey:UserDefaultsKeys.Addresses.rawValue)
        
    }
    
    func getAddresses() -> [Address] {
        var addresses = [Address]()
       
        if let data = self.value(forKey:UserDefaultsKeys.Addresses.rawValue) as? Data {
            addresses = try! PropertyListDecoder().decode(Array<Address>.self, from: data)
        }
        return addresses
     }
    
    func saveAddNewAddress() {
        
         //UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.Addresses.rawValue)
         //return
        var addresses = getAddresses()
        if addresses.contains(where: {$0.title == TSConstant.AddressTitleAddNew}) {
//            let address = Address.init(title: "Home", fullAddress: "Barotha, Uttar Pradesh", groupName: TSConstant.AddressGroupFavorites, index:0)
//            addresses.append(address)
//            saveAddress(address)
//            
//            let workAddress = Address.init(title: "Work", fullAddress: "We work, Bengaluru", groupName: TSConstant.AddressGroupFavorites, index:1)
//            addresses.append(workAddress)
//            saveAddress(workAddress)
            
        } else {
            
            let address = Address.init(title: TSConstant.AddressTitleAddNew, fullAddress: TSConstant.AddNewPlaceDescription, groupName: TSConstant.AddressGroupOthers)
            addresses.append(address)
            saveAddress(address)
        }
    }
}
