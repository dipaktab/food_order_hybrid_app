//
//  StoreVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 07/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class StoreVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func setupTableView() {
        
        tableView.register(UINib(nibName: String(describing: StoreCell.self), bundle: nil), forCellReuseIdentifier: String(describing: StoreCell.self))
        self.tableView.tableFooterView = UIView()
    }
    
    private func loadData() {
        
        tableView.reloadData()
    }
    
}

extension StoreVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: StoreCell.self), for: indexPath) as! StoreCell
        
        return cell
        
    }
    
    
    
}
