//
//  HomeVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 03/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//


import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var orderFoodContainerView: UIView!
    @IBOutlet weak var restaurentListContainerView: UIView!
    @IBOutlet weak var rewardsContainerView: UIView!
    @IBOutlet weak var offersContainerView: UIView!
    @IBOutlet weak var btnProfile: UIButton!
    
    var urlString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let pic = UserDefaults.standard.retrieveProfilePic() {
            btnProfile.setBackgroundImage(pic, for: .normal)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func profileButtonAction(_ sender: UIButton) {
        sender.spring()
        self.performSegue(withIdentifier: "showProfileScreen", sender: self)
    }
    
    @IBAction func scanQRButtonAction(_ sender: UIButton) {
        sender.spring()

        let scanner = QRCodeScannerController(cameraImage: UIImage(named: "camera"), cancelImage: UIImage(named: "cancel"), flashOnImage: UIImage(named: "flash-on"), flashOffImage: UIImage(named: "flash-off"))

        scanner.view.frame = self.view.frame
        scanner.delegate = self
        scanner.modalPresentationStyle = .fullScreen
        self.present(scanner, animated: true, completion: nil)
    }
    
    @IBAction func orderFoodAction(_ sender: UIButton) {
        orderFoodContainerView.spring()
        urlString = TSConstant.OrderFoodUrl
        showOderScreen()
    }
    
    @IBAction func showRestaurentListAction(_ sender: UIButton) {
        restaurentListContainerView.spring()
    }
    
    @IBAction func rewardsButtonAction(_ sender: Any) {
        rewardsContainerView.spring()
    }
    
    @IBAction func offersButtonAction(_ sender: Any) {
        offersContainerView.spring()
    }
    
    func showOderScreen() {
        self.performSegue(withIdentifier: "showWebView", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWebView" {
            let swiftWebVC = segue.destination as! SwiftWebVC
            swiftWebVC.urlString = urlString
        }
    }
}

extension HomeVC: QRScannerCodeDelegate {
    
    func qrScanner(_ controller: UIViewController, scanDidComplete result: String) {
        urlString = result
        showOderScreen()

    }
    
    func qrScannerDidFail(_ controller: UIViewController, error: String) {
        
    }
    
    func qrScannerDidCancel(_ controller: UIViewController) {
        
    }
}
