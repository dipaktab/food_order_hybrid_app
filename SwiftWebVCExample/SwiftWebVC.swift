//
//  SwiftWebVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 03/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//


import WebKit

 class SwiftWebVC: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var urlString = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebPage()
    }
    
    override var prefersStatusBarHidden: Bool {
          return true
      }
    
    private func loadWebPage() {
        webView.scrollView.alwaysBounceVertical = false
        webView.scrollView.bounces = false

        if !urlString.isEmpty, let url = URL(string: urlString) {
            webView.navigationDelegate = self
            webView.load(URLRequest(url: url))
            activityIndicator.startAnimating()
            
        }
     }
    
    @IBAction func backButtonAction(_ sender: Any) {
          
        if !webView.canGoBack || webView.goBack() == nil {
              self.navigationController?.popViewController(animated: true)
          }

      }
      
      @IBAction func homeButtonAction(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
      }
      
}

extension SwiftWebVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
}
