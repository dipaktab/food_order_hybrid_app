//
//  OrderStatusCell.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 14/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

protocol OrderStatusCellDelegate: class {
    func viewItemsButtonClicked()
}

class OrderStatusCell: UITableViewCell {

    @IBOutlet weak var imgvStatusIcon: UIImageView!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var btnViewItems: UIButton!
    @IBOutlet weak var lblOrderType: UILabel!
    
    weak var delegate: OrderStatusCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell( _ order: OrderStatus) {
        
        lblOrderId.text = order.orderId
        lblStatus.text = order.status.rawValue
        lblDateTime.text = order.dateTime
        btnViewItems.setTitle("View \(order.numberOfViewItems) items", for: .normal)
        lblOrderType.text = order.orderType.rawValue
        
        if order.status == .Pending {
            lblStatus.textColor = .systemBlue
            imgvStatusIcon.backgroundColor = .systemBlue
        } else if order.status == .Dispatched {
            lblStatus.textColor = .systemYellow
            imgvStatusIcon.backgroundColor = .systemYellow
        } else if order.status == .Past {
            lblStatus.textColor = .systemGreen
            imgvStatusIcon.backgroundColor = .systemGreen
        }
    }
    
    @IBAction func viewItemsAction(_ sender: UIButton) {
        sender.spring()
        
        delegate?.viewItemsButtonClicked()
    }
    
    
}
