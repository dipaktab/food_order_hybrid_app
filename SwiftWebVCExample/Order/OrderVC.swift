//
//  OrderVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 07/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class OrderVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var pendingOrders = [OrderStatus]()
    var pastOrders = [OrderStatus]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func setupTableView() {
        
        let nib = UINib(nibName: String(describing: AddressHeaderView.self), bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: String(describing: AddressHeaderView.self))
        tableView.register(UINib(nibName: String(describing: OrderStatusCell.self), bundle: nil), forCellReuseIdentifier: String(describing: OrderStatusCell.self))
        self.tableView.tableFooterView = UIView()
    }
    
    private func loadData() {
        
        let orderStatus1 = OrderStatus.init(iconName: "", orderId: "Order #20200814", status: OrderState.Pending, dateTime: "14 Aug 2020 | 10:06 AM", numberOfViewItems: "2")
        let orderStatus2 = OrderStatus.init(iconName: "", orderId: "Order #20200814", status: OrderState.Dispatched, dateTime: "14 Aug 2020 | 11:55 AM", numberOfViewItems: "4", orderType: OrderType.TakeAway)
        let orderStatus3 = OrderStatus.init(iconName: "", orderId: "Order #20202203", status: OrderState.Past, dateTime: "22 Mar 2020 | 01:30 MM", numberOfViewItems: "3", orderType: OrderType.PickUpLater)
        let orderStatus4 = OrderStatus.init(iconName: "", orderId: "Order #20190429", status: OrderState.Past, dateTime: "29 Apr 2019 | 9:13 PM", numberOfViewItems: "10", orderType: OrderType.TakeAway)
        let orderStatus5 = OrderStatus.init(iconName: "", orderId: "Order #20180107", status: OrderState.Past, dateTime: "07 Jan 2018 | 08:00 PM", numberOfViewItems: "6")

        pendingOrders = [orderStatus1, orderStatus2]
        pastOrders = [orderStatus3, orderStatus4, orderStatus5]
        
        tableView.reloadData()
    }
    
}

extension OrderVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var sections = 0
        
        if pendingOrders.count > 0 {
            sections += 1
        }
        
        if pastOrders.count > 0 {
            sections += 1
        }
        return sections
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if pendingOrders.count > 0 && section == 0 {
            return pendingOrders.count
        }
        return pastOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: OrderStatusCell.self), for: indexPath) as! OrderStatusCell
        
        var orderStatus = OrderStatus()
        if pendingOrders.count > 0 && indexPath.section == 0 {
            orderStatus = pendingOrders[indexPath.row]
        } else {
            orderStatus = pastOrders[indexPath.row]
        }
        cell.delegate = self
        cell.configureCell(orderStatus)
        return cell
        
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: AddressHeaderView.self)) as! AddressHeaderView
        
        var title = OrderState.Past.rawValue
        
        if pendingOrders.count > 0 && section == 0 {
            title = OrderState.Pending.rawValue
        }
        header.lblTitle.text = title
        return header
    }
    
}

extension OrderVC: OrderStatusCellDelegate {
    
    func viewItemsButtonClicked() {
        self.performSegue(withIdentifier: "showOrderDetailScreen", sender: self)
    }
}

