//
//  OrderStatus.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 14/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import Foundation

enum OrderState: String {
    case Pending = "Pending"
    case Past = "Past"
    case Dispatched = "Dispatched"

}

enum OrderType: String {
    case DineIn = "Dine In"
    case TakeAway = "Take Away"
    case PickUpLater = "Pick up later"

}


struct OrderStatus {
    
    var iconName = ""
    var orderId = ""
    var status = OrderState.Past
    var dateTime = ""
    var numberOfViewItems = ""
    var orderType = OrderType.DineIn

    
}
