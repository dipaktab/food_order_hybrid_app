//
//  DealsVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 07/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class DealsVC: UIViewController {
    
    static let sectionHeaderElementKind = "section-header-element-kind"
    var dataSource: UICollectionViewDiffableDataSource<Section, Deal>! = nil
    
    enum Section: String, CaseIterable {
        case promotions = "Promotions"
        case deals = "Deals"
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        configureDataSource()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}


extension DealsVC {
    func configureCollectionView() {
        
        collectionView.collectionViewLayout = generateLayout()
        collectionView.register(UINib(nibName: String(describing: DealsCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: DealsCell.self))
        
        collectionView.register(
            HeaderView.self,
            forSupplementaryViewOfKind: DealsVC.sectionHeaderElementKind,
            withReuseIdentifier: HeaderView.reuseIdentifier)
    }

  func configureDataSource() {
    dataSource = UICollectionViewDiffableDataSource
        <Section, Deal>(collectionView: self.collectionView) {
        (collectionView: UICollectionView, indexPath: IndexPath, deal: Deal) -> UICollectionViewCell? in

        guard let cell = collectionView.dequeueReusableCell(
          withReuseIdentifier: String(describing: DealsCell.self),
          for: indexPath) as? DealsCell else { fatalError("Could not create new cell") }
            
        cell.configureCell(deal)
        return cell
        
    }
    
    dataSource.supplementaryViewProvider = { (
      collectionView: UICollectionView,
      kind: String,
      indexPath: IndexPath) -> UICollectionReusableView? in
      
      guard let supplementaryView = collectionView.dequeueReusableSupplementaryView(
        ofKind: kind,
        withReuseIdentifier: HeaderView.reuseIdentifier,
        for: indexPath) as? HeaderView else { fatalError("Cannot create header view") }
      
      supplementaryView.label.text = Section.allCases[indexPath.section].rawValue
      return supplementaryView
    }

    let snapshot = snapshotForCurrentState()
    dataSource.apply(snapshot, animatingDifferences: false)
  }

  func generateLayout() -> UICollectionViewLayout {
    let layout = UICollectionViewCompositionalLayout { (sectionIndex: Int,
        layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
        self.generateDealsLayout()
    }
    return layout
  }


  func generateDealsLayout() -> NSCollectionLayoutSection {
        
    
    let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                          heightDimension: .fractionalWidth(1.0))
    let item = NSCollectionLayoutItem(layoutSize: itemSize)

    // Show one item plus peek on narrow screens, two items plus peek on wider screens
    let groupFractionalWidth = 0.46
    let groupFractionalHeight: Float = 0.7
    let groupSize = NSCollectionLayoutSize(
      widthDimension: .fractionalWidth(CGFloat(groupFractionalWidth)),
      heightDimension: .fractionalWidth(CGFloat(groupFractionalHeight)))
    
    let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitem: item, count: 1)
    group.contentInsets = NSDirectionalEdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5)

    let headerSize = NSCollectionLayoutSize(
      widthDimension: .fractionalWidth(1.0),
      heightDimension: .estimated(44))
    
    let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
      layoutSize: headerSize,
      elementKind: DealsVC.sectionHeaderElementKind,
      alignment: .top)

    let section = NSCollectionLayoutSection(group: group)
    section.boundarySupplementaryItems = [sectionHeader]
    section.orthogonalScrollingBehavior = .groupPaging

    return section
  }

  
  func snapshotForCurrentState() -> NSDiffableDataSourceSnapshot<Section, Deal> {
    let dataArray = dealsData()
    let promos = Array(dataArray.prefix(5))
    let deals = Array(dataArray.suffix(3))

    var snapshot = NSDiffableDataSourceSnapshot<Section, Deal>()
    snapshot.appendSections([Section.promotions])
    snapshot.appendItems(promos)

    snapshot.appendSections([Section.deals])
    snapshot.appendItems(deals)

    return snapshot
  }

  func dealsData() -> [Deal] {
    
    let deal1 = Deal.init(imageUrl: "Chickenjoy", timeLeft: "45", dishName: "2 pcs Chickenjoy & Burger Steak Value Meal", dealType: "Promo", price: "8.00")
    let deal2 = Deal.init(imageUrl: "Chicken_Burger", timeLeft: "50", dishName: "Chicken Burger", dealType: "Promo", price: "10.00")
    let deal3 = Deal.init(imageUrl: "Festive_Bundle", timeLeft: "60", dishName: "Festive Bundle", dealType: "Promo", price: "25.00")
    let deal4 = Deal.init(imageUrl: "Minute_Maid", timeLeft: "20", dishName: "Minute Maid Orange", dealType: "Promo", price: "2.00")
    let deal5 = Deal.init(imageUrl: "HeavenEarth_Ice", timeLeft: "30", dishName: "Heaven & Earth Ice Lemon Tea", dealType: "Promo", price: "3.00")
    let deal6 = Deal.init(imageUrl: "SpicyFries", timeLeft: "15", dishName: "Spicy Fries", dealType: "50% OFF", price: "6.00")
    let deal7 = Deal.init(imageUrl: "ChickenGardenSalad", timeLeft: "60", dishName: "Chicken Garden Salad", dealType: "10% OFF", price: "5.00")
    let deal8 = Deal.init(imageUrl: "Mayo", timeLeft: "50", dishName: "Mayo", dealType: "20% OFF", price: "2.00")
    
    let deals = [deal1, deal2, deal3, deal4, deal5, deal6, deal7, deal8]
    return deals
  }
    
}

extension DealsVC: UICollectionViewDelegate  {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.spring()
    }
    
}
