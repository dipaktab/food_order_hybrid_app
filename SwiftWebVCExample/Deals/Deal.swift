//
//  Deal.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 17/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import Foundation

struct Deal: Hashable {
    
    var imageUrl = ""
    var timeLeft = ""
    var dishName = ""
    var dealType = ""
    var price = ""


}
