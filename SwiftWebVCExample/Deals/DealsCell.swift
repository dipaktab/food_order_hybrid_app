//
//  DealsCell.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 14/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class DealsCell: UICollectionViewCell {

    @IBOutlet weak var imgvDish: UIImageView!
    @IBOutlet weak var lblDeal: UILabel!
    @IBOutlet weak var lblDishName: UILabel!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var lblPrice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell( _ deal: Deal) {
        imgvDish.image = UIImage.init(named: deal.imageUrl)
        lblDeal.text = deal.dealType
        lblDishName.text = deal.dishName
        lblTimeLeft.text = deal.timeLeft + " mins"
        lblPrice.text = "SGD " + deal.price
    }
}
