//
//  ProfileFeatureCell.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 07/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class ProfileFeatureCell: UICollectionViewCell {
    
    @IBOutlet weak var imgv: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
