//
//  EditProfileVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 10/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit
import YPImagePicker

class EditProfileVC: UIViewController {
    
    @IBOutlet weak var imgvProfilePic: UIImageView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txfFirstName: CustomTextField!
    @IBOutlet weak var txfLastName: CustomTextField!
    @IBOutlet weak var txfCountryCode: CustomTextField!
    @IBOutlet weak var txfEmail: CustomTextField!
    @IBOutlet weak var txfMobileNumber: CustomTextField!
    @IBOutlet weak var txfDOB: CustomTextField!
    @IBOutlet weak var genderSegmentControl: UISegmentedControl!
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        showDatePicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideKeyboard()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        sender.spring()
        saveData()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        sender.spring()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeProfilePicAction(_ sender: UIButton) {
        hideKeyboard()
        sender.spring()
        openCamera()
    }
    
    @IBAction func genderValueChanged(_ sender: UISegmentedControl) {
        
        hideKeyboard()
        let userDefault = UserDefaults.standard
        let gender = genderSegmentControl.selectedSegmentIndex == 0 ? TSConstant.Male : TSConstant.Female
        btnSave.isEnabled = userDefault.getGender() != gender
        
    }
    
    @IBAction func tapOnView(_ sender: Any) {
        hideKeyboard()
    }
    
    private func loadData() {
        txfCountryCode.text = "🇸🇬 +65"
        let userDefault = UserDefaults.standard
        
        txfFirstName.text = userDefault.getFirstName()
        txfLastName.text =  userDefault.getLastName()
        txfMobileNumber.text = userDefault.getMobileNumber()
        txfEmail.text = userDefault.getEmail()
        txfDOB.text = userDefault.getDOB()
        
        if userDefault.getGender() == TSConstant.Female {
            genderSegmentControl.selectedSegmentIndex = 1
        } else {
            genderSegmentControl.selectedSegmentIndex = 0
        }
        
        if let pic = userDefault.retrieveProfilePic() {
            imgvProfilePic.image = pic
        }
        
    }
    
    private func saveData() {
        
        hideKeyboard()
        
        let userDefault = UserDefaults.standard
        userDefault.setFirstName(value: txfFirstName.text ?? "")
        userDefault.setLastName(value: txfLastName.text ?? "")
        userDefault.setMobileNumber(value: txfMobileNumber.text ?? "")
        userDefault.setEmail(value: txfEmail.text ?? "")
        userDefault.setDOB(value: txfDOB.text ?? "")
        let gender = genderSegmentControl.selectedSegmentIndex == 0 ? TSConstant.Male : TSConstant.Female
        userDefault.setGender(value: gender)
        btnSave.isEnabled = false
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showAlert("", message: TSConstant.MessageProfileDataSaved)
        
    }
    
    private func isProfileDataChanged() -> Bool {
        var isChanged = false
        let userDefault = UserDefaults.standard
        
        if userDefault.getFirstName() != txfFirstName.text {
            isChanged = true
        }
        
        if userDefault.getLastName() != txfLastName.text {
            isChanged = true
        }
        
        if userDefault.getMobileNumber() != txfMobileNumber.text {
            isChanged = true
        }
        
        if userDefault.getEmail() != txfEmail.text {
            isChanged = true
        }
        
        if userDefault.getDOB() != txfDOB.text {
            isChanged = true
        }
        
        let gender = genderSegmentControl.selectedSegmentIndex == 0 ? TSConstant.Male : TSConstant.Female
        if userDefault.getGender() != gender {
            isChanged = true
        }
        
        return isChanged
    }

}

extension EditProfileVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        btnSave.isEnabled = isProfileDataChanged()
    }
    
    private func hideKeyboard() {
        view.endEditing(true)
    }
}

extension EditProfileVC {
    
    func showDatePicker() {
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: TSConstant.PickerDoneButtonTitle, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.doneDatePicker))
        doneButton.tintColor = .darkText
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: TSConstant.PickerCancelButtonTitle, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelDatePicker))
        cancelButton.tintColor = .darkText

        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        txfDOB.inputAccessoryView = toolbar
        txfDOB.inputView = datePicker
        
    }
    
    @objc func doneDatePicker() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = TSConstant.DateFormat
        txfDOB.text = formatter.string(from: datePicker.date)
        hideKeyboard()
    }
    
    @objc func cancelDatePicker() {
        hideKeyboard()
    }
}

extension EditProfileVC {
    
    private func openCamera() {
        let picker = YPImagePicker()
        picker.didFinishPicking { [unowned picker] items, _ in
            if let photo = items.singlePhoto {
                let userDefault = UserDefaults.standard
                self.imgvProfilePic.image = photo.image
                userDefault.saveProfilePic(image: photo.image)
            }
            picker.dismiss(animated: true, completion: nil)
        }
        present(picker, animated: true, completion: nil)
    }
}
