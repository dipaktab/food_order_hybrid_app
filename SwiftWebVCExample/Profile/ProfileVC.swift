//
//  ProfileVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 06/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit
import SemiModalViewController

class ProfileVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgvProfilePic: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    let featureImages = ["feedback", "notification", "save_places"]
    let featureTitles = ["Feedback", "Notification", "Served Places"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func loadData() {
        
        let userDefault = UserDefaults.standard
        var name = userDefault.getFirstName()
        
        if !name.isEmpty {
           name = name + " " + userDefault.getLastName()
        }
        
        lblUserName.text = name
        lblMobileNumber.text = userDefault.getMobileNumber()
        lblEmail.text = userDefault.getEmail()
        
        if let pic = userDefault.retrieveProfilePic() {
            imgvProfilePic.image = pic
        }
        
    }
    
    @IBAction func editProfileAction(_ sender:  UIButton) {
        sender.spring()
        self.performSegue(withIdentifier: "showEditProfile", sender: self)
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        sender.spring()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func settingsAction(_ sender: UIButton) {
        sender.spring()
        self.performSegue(withIdentifier: "showSettingScreen", sender: self)
    }
    
    @IBAction func faqAction(_ sender: UIButton) {
        sender.spring()
    }
     
    
    @IBAction func logoutAction(_ sender: UIButton) {
        sender.spring()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}


//MARK: UICollectionView Methods
extension ProfileVC: UICollectionViewDataSource, UICollectionViewDelegate {
    
    private func setupCollectionView() {
        
        collectionView.register(UINib(nibName: String(describing: ProfileFeatureCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ProfileFeatureCell.self))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return featureTitles.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ProfileFeatureCell.self), for: indexPath) as! ProfileFeatureCell
        cell.lblTitle.text = featureTitles[indexPath.row]
        
        let imageName = featureImages[indexPath.row]
        
        if imageName.isEmpty {
            cell.imgv.backgroundColor = .green
        } else {
            cell.imgv.image = UIImage.init(named: imageName)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.collectionView!.cellForItem(at: indexPath)!
        cell.spring()
        
        if indexPath.row == 0 {
            showFeedbackScreen()
        } else if indexPath.row == 2 {
            self.performSegue(withIdentifier: "showServedPlaces", sender: self)
        }
    }
    
}

extension ProfileVC {
    
    private func showFeedbackScreen() {
        
        let options: [SemiModalOption : Any] = [
            SemiModalOption.pushParentBack: false,
            SemiModalOption.animationDuration : 0.2
        ]
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let feedbackVC = storyboard.instantiateViewController(withIdentifier: "FeedbackVC")
        
    
        feedbackVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.8)
        
        presentSemiViewController(feedbackVC, options: options, completion: {
        }, dismissBlock: {
        })
    }
   
}
