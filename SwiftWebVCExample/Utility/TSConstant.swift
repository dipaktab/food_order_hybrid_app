//
//  TSConstant.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 10/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import Foundation


struct TSConstant {
    static let Male = "Male"
    static let Female = "Female"
    static let MessageProfileDataSaved = "Profile data saved successfully."
    static let MessageAddressSaved = "Address saved successfully."
    static let SentFeedbackMessage = "Feedback sent successfully."


    static let DefaultAlertTitle = "Alert!"
    static let DateFormat = "dd/MM/yyyy"
    static let PickerDoneButtonTitle = "Done"
    static let PickerCancelButtonTitle = "Cancel"
    static let OrderFoodUrl = "https://smartweb-2.tabsquare.com/scan/af92abc6-2a94-4827-b0d9-86836380cb54"
    
    //Address
    static let AddressTitleAddNew = "Add an Address"
    static let AddressTitleHome = "Home"
    static let AddressTitleWork = "Work"
    static let AddressGroupOthers = "Others"
    static let AddressGroupFavorites = "Favorites"
    static let AddNewPlaceDescription = "Save your favorite place"
    static let GoogleApiKey = "AIzaSyDU-zUerigN6y9SmSM-ir50mp0Kx9roWSE"





}
