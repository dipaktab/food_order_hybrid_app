//
//  UIView+TSAnimation.swift
//  TabsquareEmenu
//
//  Created by Dipak Pandey on 08/04/20.
//  Copyright © 2020 Tabsquare Pte. Ltd Pte. Ltd. All rights reserved.
//

import UIKit

@objc extension UIView {
    
    @objc func shrink(down: Bool) {
        UIView.animate(withDuration: 0.6) {
            if down {
                self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            }else {
                self.transform = .identity
            }
        }
    }
    
    @objc func spring() {
        
        self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            
            self.transform = .identity
        }, completion: nil)
    }
    
    
    @objc func animateSubviews() {
        animateSubviews {}
    }
    
    //Find all subviews, their subviews and add sequential spring animations.
    @objc func animateSubviews(compleationHandler: @escaping ()->Void) {
        
        for view in self.subviews {
            for subview in view.subviews {
                subview.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            }
        }
        
        var index = 0
        var completionIndex = 0
        
        for view in self.subviews {
            for subview in view.subviews {
                UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [.allowUserInteraction], animations: {
                    subview.transform = .identity
                }, completion: { _ in
                    completionIndex += 1
                    if index == completionIndex {
                        compleationHandler()
                    }
                })
                index += 1
            }
        }
    }
    
    @objc func animateTable() {
        
        if let tableView = self as? UITableView {
            
            let cells = tableView.visibleCells
            if cells.count < 1 {
                return
            }
            
            tableView.reloadData()
            
            let tableHeight: CGFloat = tableView.bounds.size.height
            
            for i in cells {
                let cell: UITableViewCell = i as UITableViewCell
                cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
            }
            
            var index = 0
            
            for a in cells {
                let cell: UITableViewCell = a as UITableViewCell
                UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [.allowUserInteraction], animations: {
                    cell.transform = .identity
                }, completion: nil)
                
                index += 1
            }
        }
    }
    
    
    @objc func animateCollectionView() {
        
        if let collectionView = self as? UICollectionView {
            let cells = collectionView.visibleCells
            if cells.count < 1 {
                return
            }
            collectionView.reloadData()
            let collectionViewHeight: CGFloat = collectionView.bounds.size.height
            
            for i in cells {
                let cell: UICollectionViewCell = i as UICollectionViewCell
                cell.transform = CGAffineTransform(translationX: 0, y: collectionViewHeight)
            }
            
            var index = 0
            
            for a in cells {
                let cell: UICollectionViewCell = a as UICollectionViewCell
                UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [.allowUserInteraction], animations: {
                    cell.transform = .identity
                }, completion: nil)
                
                index += 1
            }
        }
    }
}
