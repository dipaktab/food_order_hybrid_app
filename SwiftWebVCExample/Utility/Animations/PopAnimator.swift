//
//  PopAnimator.swift
//  TabsquareEmenu
//
//  Created by Dipak Pandey on 24/03/20.
//  Copyright © 2020 Tabsquare Pte. Ltd Pte. Ltd. All rights reserved.
//

import UIKit

// MARK: - UIViewControllerAnimatedTransitioning

@objc class PopAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    @objc let duration = 0.5
    @objc var presenting = true
    @objc var originFrame = CGRect.zero
    
    @objc var dismissCompletion: (() -> Void)?
    
  @objc func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return duration
  }
  

  @objc func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    let containerView = transitionContext.containerView
    let toView = transitionContext.view(forKey: .to) ?? UIView() //Source view
    let recipeView = presenting ? toView : transitionContext.view(forKey: .from)! //Desitantion View
    
    let initialFrame = presenting ? originFrame : recipeView.frame
    let finalFrame = presenting ? recipeView.frame : originFrame
   
    let xScaleFactor = presenting ?
      initialFrame.width / finalFrame.width :
      finalFrame.width / initialFrame.width
    
    let yScaleFactor = presenting ?
      initialFrame.height / finalFrame.height :
      finalFrame.height / initialFrame.height
    
    let scaleTransform = CGAffineTransform(scaleX: xScaleFactor, y: yScaleFactor)
    
    if presenting {
      recipeView.transform = scaleTransform
      recipeView.center = CGPoint(
        x: initialFrame.midX,
        y: initialFrame.midY)
      recipeView.clipsToBounds = true
    }
    
    recipeView.layer.cornerRadius = presenting ? 20.0 : 0.0
    recipeView.layer.masksToBounds = true
    
    containerView.addSubview(toView)
    containerView.bringSubview(toFront: recipeView)
    
    UIView.animate(
        withDuration: duration,
      delay:0.0,
      usingSpringWithDamping: 0.7,
      initialSpringVelocity: 0.0,
      options: [.allowUserInteraction],
      animations: {
        recipeView.transform = self.presenting ? .identity : scaleTransform
        recipeView.center = CGPoint(x: finalFrame.midX, y: finalFrame.midY)
        recipeView.layer.cornerRadius = !self.presenting ? 20.0 : 0.0
    }, completion: { _ in
      if !self.presenting {
        self.dismissCompletion?()
      }
      transitionContext.completeTransition(true)
    })
  }

  private func handleRadius(recipeView: UIView, hasRadius: Bool) {
    recipeView.layer.cornerRadius = hasRadius ? 20.0 : 0.0
  }
}
