//
//  UIView+IB.m
//  TabsquareEmenu
//
//  Created by Pandey Dipak on 05/01/19.
//  Copyright © 2019 Tabsquare Pte. Ltd. All rights reserved.
//

#import "UIView+IB.h"
#import <objc/runtime.h>

/**

  Top Left = kCALayerMinXMinYCorner
  Top Right = kCALayerMaxXMinYCorner
  Bottom Left = kCALayerMinXMaxYCorner
  Bottom Right = kCALayerMaxXMaxYCorner
 
 */

@implementation UIView (IB)

- (void)setUpdateShadowForceFully:(BOOL)isUpdate
{
    NSNumber *isForceUpdate = [NSNumber numberWithBool:isUpdate];
    objc_setAssociatedObject(self, @selector(updateShadowForceFully), isForceUpdate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)updateShadowForceFully
{
    NSNumber *isForceUpdate = objc_getAssociatedObject(self, @selector(updateShadowForceFully));
    return isForceUpdate.boolValue;
}

-(CGFloat)cornerRadius {
    return self.layer.cornerRadius;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    
    [self updateCorners:(kCALayerMinXMinYCorner | kCALayerMaxXMinYCorner | kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner) andRadius:cornerRadius];

}

-(CGFloat)topCornerRadius {
    return self.layer.cornerRadius;
}

- (void)setTopCornerRadius:(CGFloat)cornerRadius {
    [self updateCorners:(kCALayerMaxXMinYCorner | kCALayerMinXMinYCorner) andRadius:cornerRadius];
}

-(CGFloat)bottomCornerRadius {
    return self.layer.cornerRadius;
}

- (void)setBottomCornerRadius:(CGFloat)cornerRadius {
    [self updateCorners:(kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner) andRadius:cornerRadius];
}

-(CGFloat)leftCornerRadius {
    return self.layer.cornerRadius;
}

- (void)setLeftCornerRadius:(CGFloat)cornerRadius {
    [self updateCorners:(kCALayerMinXMinYCorner | kCALayerMaxXMinYCorner) andRadius:cornerRadius];
}

-(CGFloat)rightCornerRadius {
    return self.layer.cornerRadius;
}

- (void)setRightCornerRadius:(CGFloat)cornerRadius {
 
    [self updateCorners:(kCALayerMaxXMinYCorner | kCALayerMaxXMaxYCorner) andRadius:cornerRadius];
}

-(void)updateCorners:(CACornerMask)corners andRadius:(CGFloat)radius
{
    self.clipsToBounds = radius > 0;
    self.layer.cornerRadius = radius;
    self.layer.maskedCorners = corners;
}

- (CGFloat)borderWidth {
    return self.layer.borderWidth;
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    self.layer.borderWidth = borderWidth;
}

- (UIColor *)borderColor {
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

- (void)setBorderColor:(UIColor *)borderColor {
    self.layer.borderColor = borderColor.CGColor;
}

-(CGSize)shadowOffset {
    return self.layer.shadowOffset;
}

- (void)setShadowOffset:(CGSize)shadowOffset {
    self.layer.shadowOffset = shadowOffset;
}

-(CGFloat)shadowOpacity {
    return self.layer.shadowOpacity;
}

- (void)setShadowOpacity:(CGFloat)shadowOpacity {
    self.layer.shadowOpacity = shadowOpacity;
}

-(CGFloat)shadowRadius {
    return self.layer.shadowRadius;
}

- (void)setShadowRadius:(CGFloat)shadowRadius {
    self.layer.shadowRadius = shadowRadius;
}

- (UIColor *)shadowColor {
    return [UIColor colorWithCGColor:self.layer.shadowColor];
    
}

- (void)setShadowColor:(UIColor *)shadowColor {
    self.layer.shadowColor = shadowColor.CGColor;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if (self.updateShadowForceFully) {
        [self addShadowView];
    }
}

/*
 * Some cases corner radius and shadow effect does not work togather. In that case need to call this method.
 */

-(void)updateShadow {
    self.layer.shadowColor = self.shadowColor.CGColor;
    self.layer.shadowOffset = self.shadowOffset;
    self.layer.shadowOpacity = self.shadowOpacity;
    [[UIBezierPath bezierPathWithRoundedRect:[self bounds] cornerRadius:self.cornerRadius] CGPath];
    self.layer.masksToBounds = NO;
}

/*
 * This method add shadow effect and corner radius on caller view. It creates a UIView with same size of caller with shadow effect then insert behind the caller view.
 * Note 1- If default IB inspectable methods does not work then only call this mehtod manually.
 * Note 2- Before calling to this metod first need to set required propetries either from IB or programmatically.
 * Note 3- Do not use this method if view have show hide logic.
 */
-(void)addShadowView {
    UIView *viewShadow = [[UIView alloc] initWithFrame:self.frame];
    viewShadow.backgroundColor = [UIColor whiteColor];
    viewShadow.layer.shadowColor = self.shadowColor.CGColor;
    viewShadow.layer.shadowOffset = self.shadowOffset;
    viewShadow.layer.shadowRadius = self.shadowRadius;
    viewShadow.layer.shadowOpacity = self.shadowOpacity;
    viewShadow.layer.cornerRadius = self.cornerRadius;
    viewShadow.layer.masksToBounds = NO;
    [self.superview insertSubview:viewShadow belowSubview:self];
    
    self.layer.cornerRadius = self.cornerRadius;
    self.layer.masksToBounds = YES;
}

/*
* This method add shadow effect and corner radius on caller view.
* Note 1- If default IB inspectable methods does not work then only call this mehtod manually.
* Note 2- Before calling to this metod first need to set required propetries either from IB or programmatically.
* View inside have image view in that case may be all above will not work them use this method
*/

-(void)addShadowWithRadius {
    self.layer.cornerRadius = self.cornerRadius;
   // self.layer.masksToBounds = YES;
    
    self.layer.shadowColor = self.shadowColor.CGColor;
    self.layer.shadowOffset = self.shadowOffset;
    self.layer.shadowRadius = self.shadowRadius;
    self.layer.shadowOpacity = self.shadowOpacity;
    self.layer.masksToBounds = NO;
    
}

-(void)makeCircular {
    self.layer.cornerRadius = MIN(self.frame.size.height, self.frame.size.width) / 2.0;
    self.clipsToBounds = YES;
}
@end



