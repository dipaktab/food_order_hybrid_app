//
//  UIView+IB.h
//  TabsquareEmenu
//
//  Created by Pandey Dipak on 05/01/19.
//  Copyright © 2019 Tabsquare Pte. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


IB_DESIGNABLE
@interface UIView (IB)

@property (nonatomic) IBInspectable BOOL updateShadowForceFully;

@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat topCornerRadius;
@property (nonatomic) IBInspectable CGFloat bottomCornerRadius;
@property (nonatomic) IBInspectable CGFloat leftCornerRadius;
@property (nonatomic) IBInspectable CGFloat rightCornerRadius;




@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic, strong) IBInspectable UIColor* borderColor;

@property (nonatomic) IBInspectable CGSize shadowOffset;
@property (nonatomic) IBInspectable CGFloat shadowOpacity;
@property (nonatomic) IBInspectable CGFloat shadowRadius;
@property (nonatomic, strong) IBInspectable UIColor* shadowColor;


-(void)addShadowView;
-(void)addShadowWithRadius;
-(void)makeCircular;

@end

