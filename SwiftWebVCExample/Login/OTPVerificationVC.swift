//
//  OTPVerificationVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 06/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class OTPVerificationVC: UIViewController {

    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var txfPasscode: UITextField!
   
    var mobileNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblMobileNumber.text = mobileNumber
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
           txfPasscode.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        txfPasscode.resignFirstResponder()
        super.viewWillDisappear(animated)
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
           
           if let text = textField.text, text.count == 6 {
            self.performSegue(withIdentifier: "showHomeScreen", sender: self)
           }
       }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
