//
//  LoginVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 06/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var lblFlag: UILabel!
    @IBOutlet weak var btnSendOtp: UIButton!
    @IBOutlet weak var txfPhoneNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblFlag.text = "🇸🇬"
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        txfPhoneNumber.resignFirstResponder()
        super.viewWillDisappear(animated)
    }
    
    @IBAction func skipLoginAction(_ sender: UIButton) {
        sender.spring()
        self.performSegue(withIdentifier: "showHomeScreen", sender: self)
    }
    
    @IBAction func sendOTPAction(_ sender: UIButton) {
        sender.spring()
        self.performSegue(withIdentifier: "showOTPVerificationScreen", sender: self)
    }
    
    @IBAction func continueWithEmailAction(_ sender: UIButton) {
        sender.spring()

    }
    
    @IBAction func facebookButtonAction(_ sender: UIButton) {
        sender.spring()

    }
    
    @IBAction func googleLoginAction(_ sender: UIButton) {
        sender.spring()
    }
    
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        if let text = textField.text, text.count > 0 {
            btnSendOtp.isUserInteractionEnabled = true
            btnSendOtp.backgroundColor = .orange
        } else {
            btnSendOtp.isUserInteractionEnabled = false
            if #available(iOS 13.0, *) {
                btnSendOtp.backgroundColor = .systemGray2
            } else {
                btnSendOtp.backgroundColor = .gray
            }
        }
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showOTPVerificationScreen" {
            let otpVerificationVC = segue.destination as! OTPVerificationVC
            otpVerificationVC.mobileNumber = "+65 " + (txfPhoneNumber.text ?? "")
        }
    }
 
    
}

