//
//  SettingsVC.swift
//  SwiftWebVCExample
//
//  Created by Dipak Pandey on 14/08/20.
//  Copyright © 2020 Tabsquare Pte. Ltd. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    
    @IBOutlet weak var lblAppVersion: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblAppVersion.text = "App Version: " + (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        sender.spring()
        self.navigationController?.popViewController(animated: true)
    }


}
